package queue

import (
	"gitlab.com/eduardo.matos/gomailer/mailer"
)

// Broker is a broker which consumes/publishes email messages from a queue
type Broker interface {
	Publish(msg *mailer.MailMsg) error
	Consume() (<-chan *Msg, error)
	Close()
}

type Msg struct {
	Body []byte
}
