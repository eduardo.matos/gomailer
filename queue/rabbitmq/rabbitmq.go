package rabbitmq

import (
	"encoding/json"

	"github.com/pkg/errors"
	"github.com/streadway/amqp"
	"gitlab.com/eduardo.matos/gomailer/mailer"
	"gitlab.com/eduardo.matos/gomailer/queue"
)

// Queue is an specialized queue that deals with e-mail messages
type Queue struct {
	queueName string
	Channel   QueueChannel
	Conn      QueueConn
}

// Publish publishes a message on the queue
func (q *Queue) Publish(msg *mailer.MailMsg) error {
	jsonfiedMsg, _ := json.Marshal(msg)

	return q.Channel.Publish("", q.queueName, false, false, amqp.Publishing{
		ContentType: "application/json",
		Body:        jsonfiedMsg,
	})
}

// Consume consumes messages from the queue
func (q *Queue) Consume() (<-chan *queue.Msg, error) {
	msgs := make(chan *queue.Msg)
	deliveries, err := q.Channel.Consume(q.queueName, "", true, false, false, false, nil)

	if err != nil {
		return nil, errors.Errorf("Error while trying to consume from RabbitMQ: %s", err.Error())
	}

	go func() {
		for d := range deliveries {
			msgs <- &queue.Msg{Body: d.Body}
		}
	}()

	return msgs, nil
}

// Close closes the connection with the queue
func (q *Queue) Close() {
	q.Channel.Close()
	q.Conn.Close()
}

// New creates a new broker with the queue
func New(conn QueueConn, queueName string) (queue.Broker, error) {
	channel, err := conn.Channel()

	if err != nil {
		return &Queue{}, err
	}

	return &Queue{queueName: queueName, Conn: conn, Channel: channel}, nil
}

// QueueConn defines an interface to connect to an amqp queue
type QueueConn interface {
	Channel() (QueueChannel, error)
	Close() error
}

// QueueChannel defines an interface to publish messages on amqp
type QueueChannel interface {
	Publish(exchange, routingKey string, mandatory, immediate bool, msg amqp.Publishing) error
	Close() error
	Consume(queueName, exchange string, autoAck, exclusive, noLocal, noWait bool, args amqp.Table) (<-chan amqp.Delivery, error)
}
