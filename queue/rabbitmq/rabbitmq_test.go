package rabbitmq_test

import (
	"encoding/json"
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/eduardo.matos/gomailer/mailer"

	"github.com/streadway/amqp"
	"gitlab.com/eduardo.matos/gomailer/queue/rabbitmq"
)

type amqpMsg struct {
	exchange   string
	routingKey string
	mandatory  bool
	immediate  bool
	msg        amqp.Publishing
}

type connection struct {
	channel             *channel
	channelPublishError error
	closed              bool
}

func (c *connection) Channel() (rabbitmq.QueueChannel, error) {
	c.channel = &channel{publishError: c.channelPublishError}
	return c.channel, nil
}

func (c *connection) Close() error {
	c.closed = true
	return nil
}

type channel struct {
	published    []*amqpMsg
	publishError error
	closed       bool
	deliveries   []amqp.Delivery
	consumeArgs  []consumeArgs
	consumeErr   error
}

func (c *channel) Publish(exchange, routingKey string, mandatory, immediate bool, msg amqp.Publishing) error {
	if c.publishError != nil {
		return c.publishError
	}

	c.published = append(c.published, &amqpMsg{
		exchange:   exchange,
		routingKey: routingKey,
		mandatory:  mandatory,
		immediate:  immediate,
		msg:        msg,
	})

	return nil
}

type consumeArgs struct {
	queueName string
	exchange  string
	autoAck   bool
	exclusive bool
	noLocal   bool
	noWait    bool
	args      amqp.Table
}

func (c *channel) Consume(queueName, exchange string, autoAck, exclusive, noLocal, noWait bool, args amqp.Table) (<-chan amqp.Delivery, error) {
	c.consumeArgs = append(c.consumeArgs, consumeArgs{
		queueName: queueName,
		exchange:  exchange,
		autoAck:   autoAck,
		exclusive: exclusive,
		noLocal:   noLocal,
		noWait:    noWait,
		args:      args,
	})

	deliveries := make(chan amqp.Delivery, len(c.deliveries))

	if c.consumeErr != nil {
		return deliveries, c.consumeErr
	}

	for _, delivery := range c.deliveries {
		deliveries <- delivery
	}

	return deliveries, nil
}

func (c *channel) Close() error {
	c.closed = true
	return nil
}

func (c *channel) addDelivery(deliveries ...amqp.Delivery) {
	for _, delivery := range deliveries {
		c.deliveries = append(c.deliveries, delivery)
	}
}

func (c *channel) setConsumeErr(err error) {
	c.consumeErr = err
}

func TestPublishMessage(t *testing.T) {
	conn := &connection{}
	broker, _ := rabbitmq.New(conn, "spam")
	msg, _ := mailer.NewMsg("foo@spam.com", "spam@foo.com", "Subject here", "Message here")

	broker.Publish(msg)

	published := conn.channel.published

	assert.Equal(t, 1, len(published), "There should be only 1 message published")
	assert.Equal(t, "", published[0].exchange, `Message should be published to exhange ""`)
	assert.Equal(t, "spam", published[0].routingKey, `Message should be published to routing key "spam"`)
	assert.Equal(t, false, published[0].mandatory, "Message should not be mandatory, but it was")
	assert.Equal(t, false, published[0].immediate, "Message should not be published to routing immediatly, but it was")
	assert.Equal(t, "application/json", published[0].msg.ContentType, `Message content type should be "application/json"`)

	var publishedMailMsg mailer.MailMsg
	err := json.Unmarshal(published[0].msg.Body, &publishedMailMsg)
	if err != nil {
		assert.FailNowf(t, `Could not parse published json: %s`, string(published[0].msg.Body))
	}

	assert.EqualValues(t, *msg, publishedMailMsg, "Expected message different from published message")
}

func TestPublishReturnChannelPublishError(t *testing.T) {
	conn := &connection{channelPublishError: errors.New("Oops")}
	broker, _ := rabbitmq.New(conn, "spam")
	msg, _ := mailer.NewMsg("foo@spam.com", "spam@foo.com", "Subject here", "Message here")

	err := broker.Publish(msg)

	if err == nil {
		assert.FailNow(t, "An error should be returned when publishing message")
	}

	assert.Equal(t, err, conn.channel.publishError)
}

func TestClose(t *testing.T) {
	conn := &connection{}
	broker, _ := rabbitmq.New(conn, "spam")
	broker.Close()

	assert.Equal(t, true, conn.closed, "Connection should be closed")
	assert.Equal(t, true, conn.channel.closed, "Channel should be closed")
}

func TestConsumeArgs(t *testing.T) {
	conn := &connection{}
	broker, _ := rabbitmq.New(conn, "spam")

	msgs, err := broker.Consume()

	assert.Nil(t, err, "Consumer should not return error")
	assert.Empty(t, msgs, "There should be no deliveries")

	assert.Equal(t, conn.channel.consumeArgs, []consumeArgs{consumeArgs{
		queueName: "spam",
		exchange:  "",
		autoAck:   true,
		exclusive: false,
		noLocal:   false,
		noWait:    false,
		args:      nil,
	}})
}

func TestConsumeDeliveries(t *testing.T) {
	conn := &connection{}
	broker, _ := rabbitmq.New(conn, "spam")

	delivery1 := amqp.Delivery{Body: []byte("Foo")}
	delivery2 := amqp.Delivery{Body: []byte("Bar")}

	// simulate deliveries
	conn.channel.addDelivery(delivery1, delivery2)

	msgs, err := broker.Consume()

	if assert.Nil(t, err, "Consumer should not return error") {
		msg1 := <-msgs
		msg2 := <-msgs

		assert.Equal(t, "Foo", string(msg1.Body), "Wrong delivery body")
		assert.Equal(t, "Bar", string(msg2.Body), "Wrong delivery body")
		assert.Len(t, msgs, 0, "There should be no delivery left")
	}
}

func TestConsumeError(t *testing.T) {
	conn := &connection{}
	broker, _ := rabbitmq.New(conn, "spam")

	consumeErr := errors.New("Oops")
	conn.channel.setConsumeErr(consumeErr)

	_, err := broker.Consume()

	expectedErrorMsg := fmt.Sprintf("Error while trying to consume from RabbitMQ: %s", consumeErr.Error())
	assert.Equal(t, err.Error(), expectedErrorMsg)
}
