package web

import (
	"net/http"

	"gitlab.com/eduardo.matos/gomailer/ioc"
)

// Serve starts a new server on the specified address
func Serve(addr string, appIoc ioc.Ioc) {
	http.ListenAndServe(addr, NewRouting(appIoc).Router)
}
