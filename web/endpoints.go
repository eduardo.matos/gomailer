package web

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"gitlab.com/eduardo.matos/gomailer/mailer"

	"github.com/julienschmidt/httprouter"
)

// Response models all API responses
type Response struct {
	Message string `json:"message"`
	Success bool   `json:"success"`
}

func (rt *Routing) health(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	w.WriteHeader(http.StatusOK)
	result, _ := json.Marshal(Response{Message: "Healthy", Success: true})
	w.Write(result)
}

func (rt *Routing) sendEmail(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	r.ParseForm()
	var err error

	msg, err := mailer.NewMsg(r.FormValue("to"), r.FormValue("from"), r.FormValue("subject"), r.FormValue("msg"))

	if err != nil {
		result, _ := json.Marshal(Response{Message: fmt.Sprintf("E-mail not sent. %s", err.Error()), Success: false})
		w.WriteHeader(http.StatusBadRequest)
		w.Write(result)
		return
	}

	err = rt.Ioc.Mailer.Send(*msg)

	if err != nil {
		result, _ := json.Marshal(Response{Message: "E-mail not sent", Success: false})
		log.Printf(err.Error())
		w.WriteHeader(http.StatusBadRequest)
		w.Write(result)
		return
	}

	result, _ := json.Marshal(Response{Message: "E-mail sent", Success: true})
	w.Write(result)
}
