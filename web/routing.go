package web

import (
	"github.com/julienschmidt/httprouter"
	"gitlab.com/eduardo.matos/gomailer/ioc"
)

// Routing holds all router dependencies
type Routing struct {
	Ioc    ioc.Ioc
	Router *httprouter.Router
}

// NewRouting bootstraps a router for the application
func NewRouting(appIoc ioc.Ioc) *Routing {
	routing := &Routing{Ioc: appIoc}

	router := httprouter.New()
	router.GET("/health", routing.health)
	router.POST("/send-email", routing.sendEmail)

	routing.Router = router

	return routing
}
