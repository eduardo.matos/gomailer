package web_test

import (
	"encoding/json"
	"errors"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"

	"gitlab.com/eduardo.matos/gomailer/mailer"

	"github.com/stretchr/testify/assert"

	"gitlab.com/eduardo.matos/gomailer/ioc"
	"gitlab.com/eduardo.matos/gomailer/mailer/fake"
	"gitlab.com/eduardo.matos/gomailer/web"
)

var appIoc ioc.Ioc

func init() {
	appIoc = ioc.Ioc{Mailer: fake.New()}
}

func TestHealth(t *testing.T) {
	request, _ := http.NewRequest("GET", "/health", nil)
	response := httptest.NewRecorder()

	web.NewRouting(appIoc).Router.ServeHTTP(response, request)

	assert.Equal(t, http.StatusOK, response.Code, "Response status should be OK")

	var resp web.Response
	err := json.Unmarshal([]byte(response.Body.String()), &resp)

	if assert.NoErrorf(t, err, "Unexpected JSON format: %s", response.Body.String()) {
		expectedBody := web.Response{Message: "Healthy", Success: true}
		assert.Equal(t, expectedBody, resp)
	}
}

func TestSendEmailSuccess(t *testing.T) {
	request, _ := http.NewRequest("POST", "/send-email", strings.NewReader(url.Values{
		"to":      {"a@b.com"},
		"from":    {"b@a.com"},
		"subject": {"subject"},
		"msg":     {"body"},
	}.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	response := httptest.NewRecorder()

	routing := web.NewRouting(appIoc)
	routing.Router.ServeHTTP(response, request)

	fakeMailer := appIoc.Mailer.(*fake.Mailer)

	assert.Equal(t, 1, len(fakeMailer.Outbox), "Mail count not equal")

	expectedMail := mailer.MailMsg{To: "a@b.com", From: "b@a.com", Subject: "subject", Msg: "body"}
	assert.Equal(t, expectedMail, fakeMailer.Outbox[0])

	assert.Equal(t, http.StatusOK, response.Code, "Response status should be OK")

	var resp web.Response
	err := json.Unmarshal([]byte(response.Body.String()), &resp)

	if assert.NoErrorf(t, err, "Unexpected JSON format: %s", response.Body.String()) {
		expectedBody := web.Response{Message: "E-mail sent", Success: true}
		assert.Equal(t, expectedBody, resp, "Response body did not match expectance")
	}
}

func TestSendEmailError(t *testing.T) {
	request, _ := http.NewRequest("POST", "/send-email", strings.NewReader(url.Values{
		"to":      {"a@b.com"},
		"from":    {"b@a.com"},
		"subject": {"subject"},
		"msg":     {"body"},
	}.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	response := httptest.NewRecorder()

	fakeMailer := appIoc.Mailer.(*fake.Mailer)
	routing := web.NewRouting(appIoc)
	fakeMailer.ReturnError(errors.New(""))

	routing.Router.ServeHTTP(response, request)

	assert.Equal(t, http.StatusBadRequest, response.Code, "Response status should be Bad Request")

	var resp web.Response
	err := json.Unmarshal([]byte(response.Body.String()), &resp)

	if assert.NoErrorf(t, err, "Unexpected JSON format: %s", response.Body.String()) {
		expectedBody := web.Response{Message: "E-mail not sent", Success: false}
		assert.Equal(t, expectedBody, resp)
	}
}

func TestSendEmailInvalidTo(t *testing.T) {
	request, _ := http.NewRequest("POST", "/send-email", strings.NewReader(url.Values{
		"to":      {"com"},
		"from":    {"b@a.com"},
		"subject": {"subject"},
		"msg":     {"body"},
	}.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	response := httptest.NewRecorder()

	routing := web.NewRouting(appIoc)

	routing.Router.ServeHTTP(response, request)

	assert.Equal(t, http.StatusBadRequest, response.Code, "Response status should be Bad Request")

	var resp web.Response
	err := json.Unmarshal([]byte(response.Body.String()), &resp)

	if assert.NoErrorf(t, err, "Unexpected JSON format: %s", response.Body.String()) {
		expectedBody := web.Response{Message: `E-mail not sent. Invalid e-mail: "com"`, Success: false}
		assert.Equal(t, expectedBody, resp)
	}
}

func TestSendEmailInvalidFrom(t *testing.T) {
	request, _ := http.NewRequest("POST", "/send-email", strings.NewReader(url.Values{
		"to":      {"b@a.com"},
		"from":    {"yay"},
		"subject": {"subject"},
		"msg":     {"body"},
	}.Encode()))
	request.Header.Set("Content-Type", "application/x-www-form-urlencoded")
	response := httptest.NewRecorder()

	routing := web.NewRouting(appIoc)

	routing.Router.ServeHTTP(response, request)

	assert.Equal(t, http.StatusBadRequest, response.Code, "Response status should be Bad Request")

	var resp web.Response
	err := json.Unmarshal([]byte(response.Body.String()), &resp)

	if assert.NoErrorf(t, err, "Unexpected JSON format: %s", response.Body.String()) {
		expectedBody := web.Response{Message: `E-mail not sent. Invalid e-mail: "yay"`, Success: false}
		assert.Equal(t, expectedBody, resp)
	}
}
