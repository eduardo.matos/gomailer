package mailer

import (
	"regexp"

	"github.com/pkg/errors"
)

// Mailer defines what an e-mail sender should look like
type Mailer interface {
	Send(MailMsg) error
}

// MailMsg is a typical e-mail message
type MailMsg struct {
	From    string `json:"from"`
	To      string `json:"to"`
	Subject string `json:"subject"`
	Msg     string `json:"msg"`
}

// NewMsg creates a mail message to be sent
func NewMsg(to, from, subject, msg string) (*MailMsg, error) {
	var err error

	if err = validateEmail(to); err != nil {
		return nil, err
	}

	if err = validateEmail(from); err != nil {
		return nil, err
	}

	return &MailMsg{To: to, From: from, Subject: subject, Msg: msg}, nil
}

func validateTo(to string) error {
	if err := validateEmail(to); err != nil {
		return err
	}
	return nil
}

func validateEmail(email string) error {
	re := regexp.MustCompile("^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$")
	if !re.MatchString(email) {
		return errors.Errorf(`Invalid e-mail: "%s"`, email)
	}
	return nil
}
