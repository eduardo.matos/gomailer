package sgrid_test

import (
	"errors"
	"fmt"
	"testing"

	"github.com/sendgrid/rest"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
	"gitlab.com/eduardo.matos/gomailer/mailer/sgrid"
)

const (
	successfulResponseBody string = `{"success": true}`
	failedResponseBody     string = `{"errors": [{"message": "Oops"}, {"message": "Yay"}]}`
)

type MockedSendgridSender struct {
	outbox   []*mail.SGMailV3
	err      error
	response rest.Response
}

func (m *MockedSendgridSender) Send(email *mail.SGMailV3) (*rest.Response, error) {
	if m.err != nil {
		return &m.response, m.err
	}

	m.outbox = append(m.outbox, email)
	return &m.response, nil
}

func (m *MockedSendgridSender) returnError(err error) {
	m.err = err
}

func TestCallClient(t *testing.T) {
	client := &MockedSendgridSender{
		response: rest.Response{StatusCode: 200, Body: successfulResponseBody},
	}
	mailer := sgrid.New(client)
	err := mailer.Send("spam@google.com", "lorem@g.co", "This is a fake subject", "This is a fake message")

	if err != nil {
		t.Errorf("An error should not be returned: %v", err)
	}

	to := client.outbox[0].Personalizations[0].To[0].Address
	from := client.outbox[0].From.Address
	subject := client.outbox[0].Subject
	msg := client.outbox[0].Content[1]

	if to != "spam@google.com" {
		t.Errorf("Wrong subject: %s", subject)
	}

	if from != "lorem@g.co" {
		t.Errorf("Wrong subject: %s", subject)
	}

	if subject != "This is a fake subject" {
		t.Errorf("Wrong subject: %s", subject)
	}

	if msg.Type != "text/html" && msg.Value != "This is a fake message" {
		t.Errorf("Wrong message: (%s) %s", msg.Type, msg.Value)
	}
}

func TestClientError(t *testing.T) {
	client := &MockedSendgridSender{
		response: rest.Response{StatusCode: 200, Body: successfulResponseBody},
		err:      errors.New("Poxa vida hein, wow!"),
	}

	mailer := sgrid.New(client)
	err := mailer.Send("spam@google.com", "a@b.c", "This is a fake subject", "This is a fake message")

	if err == nil {
		t.Fatalf("An error should be returned")
	}

	expectedMessage := fmt.Sprintf("Error connecting to sendgrid: %s", client.err.Error())
	if err.Error() != expectedMessage {
		t.Errorf("Wrong error message:\nExpected: '%s'\nActual: '%s'", expectedMessage, err.Error())
	}
}

func TestClientApiError(t *testing.T) {
	client := &MockedSendgridSender{
		response: rest.Response{StatusCode: 400, Body: failedResponseBody},
	}

	mailer := sgrid.New(client)
	err := mailer.Send("spam@google.com", "b@a.c", "This is a fake subject", "This is a fake message")

	if err == nil {
		t.Fatalf("An error should be returned")
	}

	expectedMessage := fmt.Sprintf("Error sending e-mail. Errors: %s", `"Oops", "Yay"`)
	if err.Error() != expectedMessage {
		t.Errorf("Wrong error message:\nExpected: '%s'\nActual: '%s'", expectedMessage, err.Error())
	}
}

func TestClientApiMalformedJsonError(t *testing.T) {
	client := &MockedSendgridSender{
		response: rest.Response{StatusCode: 400, Body: "{"},
	}

	mailer := sgrid.New(client)
	err := mailer.Send("spam@google.com", "c@b.a", "This is a fake subject", "This is a fake message")

	if err == nil {
		t.Fatalf("An error should be returned")
	}

	expectedMessage := `Error parsing sendgrid response JSON: Error: "Key path not found" for response: "{"`
	if err.Error() != expectedMessage {
		t.Errorf("Wrong error message:\nExpected: '%s'\nActual: '%s'", expectedMessage, err.Error())
	}
}
