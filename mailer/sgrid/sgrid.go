package sgrid

import (
	"strconv"
	"strings"

	"github.com/buger/jsonparser"
	"github.com/pkg/errors"
	"github.com/sendgrid/rest"
	"github.com/sendgrid/sendgrid-go/helpers/mail"
)

// SendgridClient models the behavior of the sendgrid client
type SendgridClient interface {
	Send(email *mail.SGMailV3) (*rest.Response, error)
}

// Mailer is a sendgrid mailer
type Mailer struct {
	client SendgridClient
}

// Send sends an e-mail through sendgrid
func (m *Mailer) Send(to, from, subject, msg string) error {
	fromMail := mail.NewEmail("", from)
	toMail := mail.NewEmail("", to)
	plainTextContent := msg
	htmlContent := msg
	message := mail.NewSingleEmail(fromMail, subject, toMail, plainTextContent, htmlContent)

	response, err := m.client.Send(message)
	if err != nil {
		return errors.Errorf("Error connecting to sendgrid: %v", err)
	}

	if response.StatusCode != 200 {
		apiErrors, err := extractErrors(response.Body)

		if err != nil {
			return err
		}

		return errors.Errorf(
			"Error sending e-mail. Errors: %s",
			strings.Join(forDisplay(apiErrors...), ", "),
		)
	}

	return nil
}

func extractErrors(json string) ([]string, error) {
	apiErrors := []string{}

	_, err := jsonparser.ArrayEach([]byte(json), getErrorMessage(&apiErrors), "errors")

	if err != nil {
		return []string{}, errors.Errorf(`Error parsing sendgrid response JSON: Error: "%v" for response: "%s"`, err, json)
	}

	return apiErrors, nil
}

func getErrorMessage(errorsList *[]string) func([]byte, jsonparser.ValueType, int, error) {
	return func(value []byte, dataType jsonparser.ValueType, offset int, err error) {
		apiError, err := jsonparser.GetString(value, "message")

		if err != nil {
			return
		}

		*errorsList = append(*errorsList, apiError)
	}
}

func forDisplay(values ...string) []string {
	var dest []string

	for _, value := range values {
		dest = append(dest, strconv.Quote(value))
	}

	return dest
}

// New creates a new sendgrid mailer
func New(client SendgridClient) *Mailer {
	return &Mailer{client}
}
