package fake

import "gitlab.com/eduardo.matos/gomailer/mailer"

// Mailer is a mailer that keeps all messages in inbox
type Mailer struct {
	err    error
	Outbox []mailer.MailMsg
}

// Send fakes a message being sent
func (m *Mailer) Send(msg mailer.MailMsg) error {
	m.Outbox = append(m.Outbox, msg)
	return m.err
}

// ReturnError makes 'send' method return an error
func (m *Mailer) ReturnError(err error) {
	m.err = err
}

// New creates a new fake mailer
func New() *Mailer {
	return &Mailer{}
}
