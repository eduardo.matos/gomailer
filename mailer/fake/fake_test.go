package fake_test

import (
	"errors"
	"testing"

	"gitlab.com/eduardo.matos/gomailer/mailer"
	"gitlab.com/eduardo.matos/gomailer/mailer/fake"
)

func TestFakeMailer(t *testing.T) {
	msg, _ := mailer.NewMsg("spam@google.com", "c@b.a", "It's a subject", "This is a message")

	mail := fake.New()
	mail.Send(*msg)

	if mail.Outbox[0].To != "spam@google.com" ||
		mail.Outbox[0].From != "c@b.a" ||
		mail.Outbox[0].Msg != "This is a message" ||
		mail.Outbox[0].Subject != "It's a subject" {
		t.Errorf(
			"Message did not match. To: '%s', Subject: '%s', Msg: '%s'",
			mail.Outbox[0].To,
			mail.Outbox[0].Subject,
			mail.Outbox[0].Msg)
	}
}

func TestFakeMailerError(t *testing.T) {
	expectedError := errors.New("Oops")
	msg, _ := mailer.NewMsg("spam@google.com", "c@b.a", "It's a subject", "This is a message")

	mail := fake.New()
	mail.ReturnError(expectedError)

	err := mail.Send(*msg)

	if err != expectedError {
		t.Errorf("Expected error not returned: %s", err.Error())
	}
}
