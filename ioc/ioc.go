package ioc

import "gitlab.com/eduardo.matos/gomailer/mailer"

// Ioc is an inversion of control container for the app
type Ioc struct {
	Mailer mailer.Mailer
}
