package templating

// TemplateRenderer tells that a given struct can render a template
type TemplateRenderer interface {
	Render(templateNameOrText string, vars interface{}) (string, error)
}
