package text_test

import (
	"html/template"
	"regexp"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/eduardo.matos/gomailer/templating/text"
)

func TestRenderText(t *testing.T) {
	renderer := text.New()
	result, err := renderer.Render("<div>{{.}}</div>", "Something")

	if assert.NoError(t, err, "Error parsing template") {
		assert.Equal(t, "<div>Something</div>", result, "Could not render template properly")
	}
}

func TestRenderHtml(t *testing.T) {
	renderer := text.New()
	result, err := renderer.Render("<div>{{.}}</div>", template.HTML("<span>1</span>"))

	if assert.NoError(t, err, "Error parsing template") {
		assert.Equal(t, "<div><span>1</span></div>", result, "Could not render template properly")
	}
}

func TestMalformedTemplate(t *testing.T) {
	renderer := text.New()
	result, err := renderer.Render("<div>{{.}</div>", template.HTML("<span>1</span>"))

	assert.Error(t, err, "Should return error when there is a syntax error")
	assert.Equal(t, "", result, "Result should be blank when an error is returned")
	assert.Regexp(t, regexp.MustCompile("^Could not parse template:.+"), err.Error(), "Wrong error message")
}

func TestTemplateExecutionError(t *testing.T) {
	renderer := text.New()
	result, err := renderer.Render("<div>{{.Spam}}</div>", "Oops")

	assert.Error(t, err, "An error should be returned when template can't be executed")
	assert.Equal(t, "", result, "Result should be blank when an error is returned")
	assert.Regexpf(t, regexp.MustCompile("^Problem executing template:.+"), err.Error(), "Wrong error message: %s", err.Error())
}
