package text

import (
	"bytes"
	"html/template"

	"github.com/pkg/errors"
)

// Renderer is a text renderer
type Renderer struct{}

// Render renders template from string
func (r *Renderer) Render(templateText string, vars interface{}) (string, error) {
	tpl, err := template.New("").Parse(templateText)

	if err != nil {
		return "", errors.Errorf("Could not parse template: %v", err)
	}

	result := bytes.Buffer{}
	err = tpl.Execute(&result, vars)

	if err != nil {
		return "", errors.Errorf("Problem executing template: %v", err)
	}

	return result.String(), nil
}

// New creates a new text renderer
func New() *Renderer {
	return &Renderer{}
}
