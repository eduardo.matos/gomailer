package directory_test

import (
	"path/filepath"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/eduardo.matos/gomailer/templating/directory"
)

func TestRenderTemplate(t *testing.T) {
	path, err := filepath.Abs("./dummy_templates")

	assert.Nil(t, err, "Could not locate templates")

	directoryRenderer, err := directory.NewRenderer(path)
	result, err := directoryRenderer.Render("with-bold-text", "Custom message")

	assert.Nilf(t, err, "Error rendering 'with-bold-text' template. Does it exist? %v", err)
	assert.Equal(t, result, "<strong>Custom message</strong>", "Error rendering message")
}

func TestReturnsErrorIfDirectoryDoesntExist(t *testing.T) {
	_, err := directory.NewRenderer("./foo/bar/ba")

	assert.NotNil(t, err, "Error should not be nil since the given diretory does not exist")
}

func TestOnlyParseFilesWithGohtmlExtension(t *testing.T) {
	directoryRenderer, err := directory.NewRenderer("./dummy_templates")

	assert.Nilf(t, err, "templates directory not found: %v", err)

	result, err := directoryRenderer.Render("raw", "Spam")

	if assert.Error(t, err) {
		assert.Equalf(t, "", result,
			`Result should be an empty string and error should not be nil when template is not found.
	Result: "%v"
	Error: "%v"`,
			result,
			err,
		)
	}
}
