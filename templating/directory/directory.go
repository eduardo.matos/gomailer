package directory

import (
	"bytes"
	"html/template"
	"path/filepath"
)

// Renderer is an specialized template renderer that gets the templates from a directory
type Renderer struct {
	tpl *template.Template
}

// Render renders a given template. Templates should have the extension ".gohtml"
func (dr *Renderer) Render(templateName string, vars interface{}) (string, error) {
	result := bytes.Buffer{}

	if err := dr.tpl.ExecuteTemplate(&result, templateName+".gohtml", vars); err != nil {
		return "", err
	}

	return result.String(), nil
}

// NewRenderer creates a new directory renderer
func NewRenderer(templtesPath string) (*Renderer, error) {
	tpl, err := template.ParseGlob(filepath.Join(templtesPath, "*.gohtml"))

	if err != nil {
		return &Renderer{}, err
	}

	return &Renderer{tpl: tpl}, nil
}
